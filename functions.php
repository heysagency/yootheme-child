<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
      
if ( !function_exists( 'child_theme_configurator_css' ) ):
    function child_theme_configurator_css() {
        wp_enqueue_style( 'chld_thm_cfg_separate', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array() );
        //wp_enqueue_script( 'script-name', trailingslashit( get_stylesheet_directory_uri() ) . 'theme.js', array(), '1.0.0', true );
    }
endif;
add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css', 20 );

// Woo - remove breadcrumbs
add_action('template_redirect', 'remove_shop_breadcrumbs' );
function remove_shop_breadcrumbs(){
 
    if (is_product())
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
 
}

// Woo Sidecart - hide when empty
add_action( 'wp_footer', function() {
    
    if ( WC()->cart->is_empty() ) {
        
        echo '<style type="text/css">.xoo-wsc-basket{ display: none; }</style>';
    
    }

});

// Removes Order Notes Title - Additional Information & Notes Field
add_filter( 'woocommerce_enable_order_notes_field', '__return_false', 9999 );

// Remove Order Notes Field
add_filter( 'woocommerce_checkout_fields' , 'remove_order_notes' );

function remove_order_notes( $fields ) {
     unset($fields['order']['order_comments']);
     return $fields;
}

// Disabling Automatic Scrolling On All Gravity Forms
add_filter( 'gform_confirmation_anchor', '__return_false' );